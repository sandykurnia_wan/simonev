<section class="content-header">
        <?php echo $this->session->flashdata('pesan') ?>
    <div>
    <h1>
        <small></small>
    </h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
        <li class="active">Tambah User</li>
    </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"> Add Dosen </h3>
    </div>
    <div class="box-content">
      <form action ="<?php echo base_url('admin/insert_user')?>" class="form-horizontal" method="post" id="form_dosen">
          <div class="form-group">
              <label class="col-sm-2 control-label">Username : </label>
              <div class="col-sm-9">
                  <input type="text" class="form-control"  placeholder="Masukkan NIP" name="NIP" >
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Nama : </label>
              <div class="col-sm-9">
                  <input type="text" class="form-control"  placeholder="Masukkan Nama" name="nama" >
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Role : </label>
            <div class="col-sm-2">
              <input type="text" class="form-control" name="role" value="Dosen" readonly>
            </div>
          </div>
          <div class="form-group">
              <div class="col-sm-11">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="reset" class="btn btn-danger pull-right">Reset</button>
              </div>
          </div>
      </form>
    </div>
    <div class="box-footer">

    </div>
  </div>
</section>
<script type="text/javascript">

</script>
