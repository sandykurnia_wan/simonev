<form action ="<?php echo base_url('mahasiswa/editpending')?>" class="form-horizontal" method="post">
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama :</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $pending[0]['Mahasiswa']?>" placeholder="Masukkan judul PKL Anda" name="judul" required readonly>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">NIM :</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $pending[0]['NIM']?>" placeholder="Masukkan Perusahaan/ Instansi PKL" name="perusahaan" required readonly>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal :</label>
            <div class="col-sm-9">
                <input type="date" class="form-control" value="<?php echo $pending[0]['tanggal']?>" placeholder="Masukkan tanggal kegiatan" name="tanggal"  required>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Topik :</label>
            <div class="col-sm-9">
                <select class="form-control" required name="idtopik" >
                        <option value="NULL">--- Pilih topik kegiatan ---</option>
                        <option <?php echo ($pending[0]['idtopik'] == "1")?"selected":"" ?> value="1">Topik/ Judul</option>
                        <option <?php echo ($pending[0]['idtopik'] == "2")?"selected":"" ?> value="2">Proposal</option>
                        <option <?php echo ($pending[0]['idtopik'] == "3")?"selected":"" ?> value="3">Lapangan</option>
                        <option <?php echo ($pending[0]['idtopik'] == "4")?"selected":"" ?> value="4">Laporan</option>
                        <option <?php echo ($pending[0]['idtopik'] == "5")?"selected":"" ?> value="5">Seminar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Kegiatan :</label>
            <div class="col-sm-9">
                <textarea class="form-control" placeholder="Jelaskan kegiatan yang Anda lakukan" name="kegiatan" required=""><?php echo $pending[0]['Kegiatan']?></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-warning pull-right">Reset</button>            
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>            
        </div>
    </div>
</form>           