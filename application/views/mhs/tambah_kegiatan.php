<form action ="<?php echo base_url('mahasiswa/insertKegiatan')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal : </label>
        <div class="col-sm-9">
            <input type="date" class="form-control"  placeholder="Masukkan tanggal kegiatan" name="tanggal"  required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Topik : </label>
        <div class="col-sm-9">
            <select class="form-control" required name="idtopik">
                        <option value="NULL">--- Pilih topik kegiatan ---</option>
                        <option value="1">Topik/ Judul</option>
                        <option value="2">Proposal</option>
                        <option value="3">Lapangan</option>
                        <option value="4">Laporan</option>
                        <option value="5">Seminar</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Kegiatan : </label>
        <div class="col-sm-9">
            <textarea class="form-control" placeholder="Jelaskan kegiatan yang Anda lakukan" name="kegiatan" required=""></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-11">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
            
        </div>
    </div>
</form>           