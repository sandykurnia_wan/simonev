<!-- Content Header (Page header) -->
<section class="content-header">
        <?php echo $this->session->flashdata('pesan') ?>
    <div>
    <h1>
        <!-- Daftar User -->
        <small></small>
    </h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mahasiswa</a></li>
        <li class="active">Home</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> Basic Information </h3>
        </div>
        <div class="box-body">
        <!-- Data PKL (judul sama perusahaan) -->
            <br/>
           
            <div class="atas">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul PKL </label>
                        <label class="col-sm-10 control-label">: <?php echo $basic[0]['Judul']?></label>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perusahaan/ Instansi</label>
                        <label class="col-sm-10 control-label">: <?php echo $basic[0]['Perusahaan']?></label>
                    </div>
                </div>
                <br>
                <div class="col-sm-12">
                    <button  class="btn btn-primary pull-right" data-toggle="modal" data-target="#basic">
                        <i class="fa fa-edit"></i> Edit
                    </button>
                </div>
                <br> <br>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <!-- status kegiatan terakhir -->
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
    <!-- box ke 2 -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> Kegiatan terakhir </h3>
        </div>
        <div class="box-body">
            <?php 
                if(!$pending){
                    echo 'Tidak ada kegiatan dengan status pending';
                } else {
                    $this->load->view('mhs/kegiatan_pending');
                }
            ?>
            <!-- Kegiatan yang masih pending -->
            
        </div><!-- /.box-body -->
        <div class="box-footer">
            <!-- status kegiatan terakhir -->
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
         <!-- Modal Basic -->
            <div class="modal fade" id="basic" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit basic</h4>
                        </div>
                        <div class="modal-body">
                            <?php $this->load->view('mhs/edit_basic'); ?>  
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Pending -->
            <div class="modal fade" id="pending" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit kegiatan terakhir</h4>
                        </div>
                        <div class="modal-body">
                            <?php $this->load->view('mhs/edit_pending'); ?>  
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>            


    <script type="text/javascript">
        function confirmDelete(){
            conf = confirm("Anda Yakin Akan Menghapus Data ?");
            if (conf == true) 
                return true;
            else 
                return false;
        }
    </script>
</section><!-- /.content -->
