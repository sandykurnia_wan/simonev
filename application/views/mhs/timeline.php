<!-- Content Header (Page header) -->
<section class="content-header">
        <?php echo $this->session->flashdata('pesan') ?>
    <div>
    <h1>
        <!-- Daftar User -->
        <small></small>
    </h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mahasiswa</a></li>
        <li class="active">Timeline</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<br>
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> Timeline kegiatan </h3>
        </div>
        <div class="box-body">
        <section class="cd-horizontal-timeline">
        	<div class="timeline">
        		<div class="events-wrapper">
        			<div class="events">
        				<ol>
        					<?php 
        						foreach ($log as $l){ 
        							$tgl = date_create($l['tanggal']);
									$tgl2 = date_format($tgl,'d/m/y');
									echo $tgl2;
        					?>
        					<li><a href="#0" <?php //echo ($pending[0]['idtopik'] == "1")?"class='selected'":"" ?> data-date="<?php echo $tgl2?>"><?php echo $tgl2?></a></li>
        					<?php 	
        						}
        					?>
        				</ol>

        				<span class="filling-line" aria-hidden="true"></span>
        			</div> <!-- event -->
        		</div> <!-- event wrapper -->
        		<ul class="cd-timeline-navigation">
	        		<li><a href="#0" class="prev inactive">Prev</a></li>
					<li><a href="#0" class="next">Next</a></li>
	        	</ul>
        	</div><!-- timeline-->

        	<div class="events-content">
        		<ol>
        			<?php 
        				foreach ($log as $l){ 
        			?>
        			<li class="selected" data-date="<?php echo $l['tanggal']?>">
        				<h2><?php echo $l['judul']?></h2>
        				<em><?php echo $l['tanggal']?></em>
        				<p>
        					<?php echo $l['Kegiatan']?>
        				</p>
        			</li>

  					<?php 	
        				}
        			?>
        			<li class="selected" data-date="16/01/2014">
        				<h2>judul</h2>
        				<em>January 16 2014</em>
        				<p>
        					isinya
        				</p>
        			</li>
        		</ol>
        	</div> <!-- event contents-->
        </section>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <!-- status kegiatan terakhir -->
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <script type="text/javascript">
        function confirmDelete(){
            conf = confirm("Anda Yakin Akan Menghapus Data ?");
            if (conf == true) 
                return true;
            else 
                return false;
        }
    </script>
</section><!-- /.content -->
