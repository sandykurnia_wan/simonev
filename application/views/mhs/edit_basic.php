<form action ="<?php echo base_url('mahasiswa/editbasic')?>" class="form-horizontal" method="post">
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-3 control-label">Judul PKL :</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" value="<?php echo $basic[0]['Judul']?>" placeholder="Masukkan judul PKL Anda" name="judul" required>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-3 control-label">Perusahaan/ Instansi :</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" value="<?php echo $basic[0]['Perusahaan']?>" placeholder="Masukkan Perusahaan/ Instansi PKL" name="perusahaan" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-warning pull-right">Reset</button>            
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>            
        </div>
    </div>
</form>           