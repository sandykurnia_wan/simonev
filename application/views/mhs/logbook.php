<!-- Content Header (Page header) -->
<section class="content-header">
        <?php echo $this->session->flashdata('pesan') ?>
    <div>
    <h1>
        <!-- Daftar User -->
        <small></small>
    </h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mahasiswa</a></li>
        <li class="active">Logbook</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> Logbook </h3>
        </div>
        <div class="box-body">
            <!-- Trigger the modal with a button -->
            <?php 
                if ($pending == 0){
                    $button = '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Tambah kegiatan baru</button>';
                     echo $button;
                } 
            ?>
            <br/>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah kegiatan baru</h4>
                        </div>
                        <div class="modal-body">
                            <?php $this->load->view('mhs/tambah_kegiatan'); ?>  
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Detail view -->
            <div class="modal fade" id="myModal1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Detail kegiatan</h4>
                        </div>
                        <div class="modal-body">
                            <?php //$this->load->view('mhs/tambah_kegiatan'); ?>  
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <br/>
            <table id="user1" class="table table-bordered table-hover col-xs-pull-right">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Topik</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; 
                    foreach($log as $d){ ?>
                    <tr>
                        <td> <?php echo $i ?></td>
                        <td><?php echo $d['judul'] ?></td>
                        <td><?php echo $d['tanggal'] ?></td>
                        <td><?php echo $d['Status'] ?></td>
                        <td> 
                            <a  href="<?php //echo base_url()."admin/edit/".$d['username']; ?>" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal1"><i class="fa fa-fw fa-edit"></i>Detail</a> 
                        </td>
                    </tr>
                    <?php 
                    $i = $i+1; } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
    <script type="text/javascript">
        function confirmDelete(){
            conf = confirm("Anda Yakin Akan Menghapus Data ?");
            if (conf == true) 
                return true;
            else 
                return false;
        }
    </script>
</section><!-- /.content -->
