<div class="bawah">
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama </label>
            <label class="col-sm-7 control-label">: <?php echo $pending[0]['Mahasiswa']?></label>
            <div class="col-sm-3">
                <button  class="btn btn-warning pull-right">
                <i class="fa fa-star"></i>  Status : <?php echo $pending[0]['Status']?>
                </button>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">NIM </label>
            <label class="col-sm-7 control-label">: <?php echo $pending[0]['NIM']?></label>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal </label>
            <label class="col-sm-7 control-label">: <?php echo $pending[0]['tanggal']?></label>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Topik </label>
            <label class="col-sm-7 control-label">: <?php echo $pending[0]['judul']?></label>
        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label">Kegiatan </label>
            <label class="col-sm-7 control-label">: <?php echo $pending[0]['Kegiatan']?></label>
        </div>
    </div>
    <div class="col-sm-12">
        <button  class="btn btn-primary pull-right" data-toggle="modal" data-target="#pending">
        <i class="fa fa-edit"></i> Edit
        </button>
    </div>
    <br>
    <br>
</div>