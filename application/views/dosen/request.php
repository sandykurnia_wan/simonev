<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        <!-- Request
        <small>it all starts here</small> -->
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Request Bimbingan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"> Daftar Request Mahasiswa</h3>
    </div>
    <div class="box-body">
        <table id="user2" class="table table-bordered table-hover col-xs-pull-right" style="text-align:center">
            <thead>
                <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">Nama</th>
                    <th style="text-align:center">NIM</th>
                    <th style="text-align:center">Angkatan</th>
                    <th style="text-align:center">Status</th>
                    <th style="text-align:center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; 
                foreach($req as $d){ ?>
                <tr>
                    <td> <?php echo $i ?></td>
                    <td><?php echo $d['Mahasiswa'] ?></td>
                    <td><?php echo $d['NIM'] ?></td>
                    <td><?php echo $d['Angkatan'] ?></td>
                    <td><?php echo $d['Status'] ?></td>
                    <td style="text-align:center"> 
                        <a  href="<?php //echo base_url()."admin/edit/".$d['username']; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-list"></i>&nbsp &nbsp Detail</a> 
                    </td>
                </tr>
                <?php 
                    $i = $i+1; } 
                ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        
    </div>
</div>



</section><!-- /.content -->
