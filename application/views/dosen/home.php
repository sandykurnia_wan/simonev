<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Dashboard
        <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="col-lg-6 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3>12419<?php //echo $this->madmin->countUser()?></h3>
            <p>Mahasiswa</p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
        <a href="<?php echo site_url('admin/user') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-6 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
        <div class="inner">
            <h3>79124<?php //echo $this->madmin->countUser()?></h3>
            <p>Mahasiswa Seminar</p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
        <a href="<?php echo site_url('admin/user') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<br><br><br><br><br><br><br>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"> Daftar Mahasiswa bimbingan</h3>
    </div>
    <div class="box-body">
        <table id="user1" class="table table-bordered table-hover col-xs-pull-right" style="text-align:center">
            <thead>
                <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">NIM</th>
                    <th style="text-align:center">Nama</th>
                    <th style="text-align:center">Angkatan</th>
                    <th style="text-align:center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; 
                foreach($mhs as $d){ ?>
                <tr>
                    <td> <?php echo $i ?></td>
                    <td><?php echo $d['NIM'] ?></td>
                    <td><?php echo $d['Mahasiswa'] ?></td>
                    <td><?php echo $d['Angkatan'] ?></td>
                    <td style="text-align:center"> 
                        <a  href="<?php //echo base_url()."admin/edit/".$d['username']; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-list"></i>&nbsp &nbsp Detail</a> 
                    </td>
                </tr>
                <?php 
                    $i = $i+1; } 
                ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        
    </div>
</div>



</section><!-- /.content -->
