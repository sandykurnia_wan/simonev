<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDosen extends CI_Model {
	
	public function getMhs($id){
		$this->db->where('NIP', $id);
		$res = $this->db->get('mahasiswa');
		return $res->result_array();
	}

	public function getReq($id){
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->join('logbook', 'logbook.NIM = mahasiswa.NIM');
		$this->db->join('topik', 'logbook.idtopik = topik.idtopik');
		$this->db->where('mahasiswa.NIP', $id);
		$this->db->where('Status', 3);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getUser($params){
		$query = 'SELECT * FROM user WHERE username ="'.$params.'"';
        $res = $this->db->query($query);
		// $data = $this->db->query('select * from '.$table.' '.$where);
		return $res->result_array();
	}

	public function updateUser($data, $id){
		// $res = $this->db->update($tabel, $data, $where);
		$this->db->where('username', $id);
		$res = $this->db->update('user', $data);
		// $res = $this->db->affected_rows();
		return $res;
	}

	public function updateData($data, $id){
		// $res = $this->db->update($tabel, $data, $where);
		$this->db->where('email1', $id);
		$res = $this->db->update('pribadi', $data);
		// $res = $this->db->affected_rows();
		return $res;
	}

	public function getData($table = "", $id){
		$this->db->where('email1', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function getLog($id){
		$this->db->select('logged');
		$this->db->where('username', $id);
		$res = $this->db->get('user');
		return $res->result_array();
	}
}