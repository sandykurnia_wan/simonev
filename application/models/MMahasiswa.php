<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MMahasiswa extends CI_Model {
	
	public function updateJudul($data, $id){
		//insert judul, perusahaan, angkatan
		$this->db->where('NIM', $id);
		$res = $this->db->update('mahasiswa', $data);
	}

	public function getlogbook($id){
		$this->db->select('*');
		$this->db->from('logbook');
		$this->db->join('topik', 'topik.idtopik = logbook.idtopik');
		$this->db->where('NIM', $id);
		$this->db->where('Status', 1);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getBasic($id){
		$this->db->where('NIM', $id);
		$res = $this->db->get('mahasiswa');
		return $res->result_array();
	}

	public function getPending($id){
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->join('logbook', 'logbook.NIM = mahasiswa.NIM');
		$this->db->join('topik', 'logbook.idtopik = topik.idtopik');
		$this->db->where('mahasiswa.NIM', $id);
		$this->db->where('Status', 3);
		$res = $this->db->get();
		return $res->result_array();
	}

	public function cekPending($id){
		$this->db->where('NIM', $id);
		$this->db->where('Status', 3);
		$res = $this->db->get('logbook');
		// $data = $this->db->query('select * from '.$table.' '.$where);
		return $res->num_rows();
	}

	public function getMhs($id){
		$this->db->where('NIM', $id);
		$res = $this->db->get('mahasiswa');
		return $res->result_array();
	}

	public function insertKegiatan($data){
		$res = $this->db->insert('logbook', $data);
		return $res;
	}

	public function updatePending($data, $id){
		$this->db->where('NIM', $id);
		$this->db->where('Status', 3);
		$res = $this->db->update('logbook', $data);
	}

	public function updateData($data, $id){
		// $res = $this->db->update($tabel, $data, $where);
		$this->db->where('username', $id);
		$res = $this->db->update('user', $data);
		// $res = $this->db->affected_rows();
		return $res;
	}

	public function delete($tabel, $where){
		$res = $this->db->delete($tabel, $where);
		return $res;
	}

	public function countUser(){
		$query = 'SELECT * FROM user WHERE Role = 4 ';
        $data = $this->db->query($query);
		// $data = $this->db->query('select * from '.$table.' '.$where);
		return $data->num_rows();
	}

	
}