<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('role') == 'Admin') {
			redirect('admin');
		} else if ($this->session->userdata('role') == 'Departemen') {
			redirect('departemen');
		} else if ($this->session->userdata('role') == 'Dosen') {
			redirect('dosen');
		} else if ($this->session->userdata('role') == 'Mahasiwa') {
			redirect('mahasiswa');
		}
		// } else if ($this->session->userdata('logged_in' == '0')) {
		// 	redirect('auth/firstlogin');
		// }
		$this->load->helper('text');
	}

	public function index() {
		$this->load->view('login');
	}

	public function cek_login() {
		$data = array(
			'username' => $this->input->post('username', TRUE),
			'password' => ($this->input->post('password', TRUE))
		);
		$this->load->model('mauth'); // load model
		$hasil = $this->mauth->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['role'] = $sess->Role;
				$sess_data['iduser'] = $sess->Id_User;
				$sess_data['username'] = $sess->Username;
				$this->session->set_userdata($sess_data);
			}
			// print_r($hasil);die;
			if ($this->session->userdata('role') == 'Admin') {
				redirect('admin');
			} else if ($this->session->userdata('role') == 'Departemen') {
				redirect('departemen');
			} else if ($this->session->userdata('role') == 'Dosen') {
				redirect('dosen');				
				// if ($this->session->userdata('logged_in') == '0'){
				// 	redirect('dosen/firstlogin');
				// } else {
				// 	redirect('dosen');
				// }
			} else if ($this->session->userdata('role') == 'Mahasiswa') {
				redirect('mahasiswa');
				// if ($this->session->userdata('logged_in') == '0'){
				// 	redirect('Mahasiswa/firstlogin');
				// } else {
				// 	redirect('Mahasiswa');
				// }
			}
		
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}

?>