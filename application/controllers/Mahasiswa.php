<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		} else if ($this->session->userdata('role') == '1') {
			redirect('admin');
		} else if ($this->session->userdata('role') == '2') {
			redirect('departemen');
		} else if ($this->session->userdata('role') == '3') {
			redirect('dosen');
		}
		$this->load->helper('text');
		$this->load->model('mmahasiswa');
	}

	public function index() {
		$data['title']='Home | Mahasiswa';
		//$this->load->view('dashboard1');
		$id = $this->session->userdata('username');
		$data['basic'] = $this->mmahasiswa->getBasic($id);
		$data['pending'] = $this->mmahasiswa->getPending($id);
		$this->template->load('TMahasiswa','mhs/home',$data);
	}

	public function logbook() {
		$data['title']='Logbook | Mahasiswa';
		//$this->load->view('dashboard1');
		$id = $this->session->userdata('username');
		$data['log'] = $this->mmahasiswa->getlogbook($id);
		$data['pending'] = $this->mmahasiswa->cekPending($id);
		// print_r($data['pending']);die;
		$this->template->load('TMahasiswa','mhs/logbook',$data);
	}

	public function timeline() {
		$data['title']='Timeline | Mahasiswa';
		$id = $this->session->userdata('username');
		$data['log'] = $this->mmahasiswa->getlogbook($id);
		$this->template->load('TMahasiswa','mhs/timeline',$data);
	}

	public function insertKegiatan(){
		//ambil dt mhsnya
		$id = $this->session->userdata('username');
		$mhs = $this->mmahasiswa->getMhs($id);
		//set variable nim, nip, status, 
		//topik, tanggal sama kegiatan dari input post
		$nip = $mhs[0]['NIP'];
		$topik = $this->input->post('idtopik');
		$tanggal = $this->input->post('tanggal');
		$kegiatan = $this->input->post('kegiatan');
		//set input ke database
		$data_insert = array(
					'NIM' 		=> $id,
					'NIP' 		=> $nip,
					'Status' 	=> 3,
					'idtopik'	=> $topik,
					'tanggal' 	=> $tanggal,
					'Kegiatan' 	=> $kegiatan
				);
		$res = $this->mmahasiswa->insertKegiatan($data_insert);
		if ($res >=1){
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-success" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>User baru berhasil ditambahkan!</strong>
				</div>');
			redirect('mahasiswa/logbook');
		} else {
			//echo "<script>alert('User gagal bertambah');</script>";
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-danger" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>User Gagal bertambah</strong>
				</div>');
			redirect('mahasiswa/logbook');
		}
	}

	public function editBasic(){
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$perusahaan = $this->input->post('perusahaan');
		$data_insert = array(
					'Judul' 		=> $judul,
					'Perusahaan' 	=> $perusahaan
				);
		$res = $this->mmahasiswa->updateJudul($data_insert, $id);
		if (!$res){
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-success" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>Data berhasil diubah</strong>
				</div>');
			redirect('mahasiswa');
		} else {
			//echo "<script>alert('User gagal bertambah');</script>";
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-danger" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>Data gagal diubah</strong>
				</div>');
			redirect('mahasiswa');
		}
	}

	public function editPending(){
		$id = $this->session->userdata('username');
		$tanggal = $this->input->post('tanggal');
		$idtopik = $this->input->post('idtopik');
		$kegiatan = $this->input->post('kegiatan');
		$data_insert = array(
					'tanggal' 	=> $tanggal,
					'idtopik' 	=> $idtopik,
					'Kegiatan'	=> $kegiatan
				);
		$res = $this->mmahasiswa->updatePending($data_insert, $id);
		if (!$res){
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-success" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>Data berhasil diubah</strong>
				</div>');
			redirect('mahasiswa');
		} else {
			//echo "<script>alert('User gagal bertambah');</script>";
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-danger" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<strong>Data gagal diubah</strong>
				</div>');
			redirect('mahasiswa');
		}	
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}
}
?>