<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departemen extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		} else if ($this->session->userdata('role') == '3') {
			redirect('dosen');
		} else if ($this->session->userdata('role') == '1') {
			redirect('admin');
		} else if ($this->session->userdata('role') == '4') {
			redirect('mahasiswa');
		}
		$this->load->helper('text');
		$this->load->model('mdepartemen');
		$this->load->library('email');
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}

?>