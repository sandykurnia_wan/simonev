<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		} else if ($this->session->userdata('role') == '1') {
			redirect('admin');
		} else if ($this->session->userdata('role') == '2') {
			redirect('departemen');
		} else if ($this->session->userdata('role') == '4') {
			redirect('mahasiswa');
		}
		$this->load->helper('text');
		$this->load->model('mdosen');
	}

	public function index(){
		$data['title'] ='Home | Dosen';
		$id = $this->session->userdata('username');
		$data['mhs'] = $this->mdosen->getMhs($id);
		$this->template->load('TDosen','dosen/home',$data);
	}

	public function request(){
		$data['title'] ='Request Bimbingan | Dosen';
		$id = $this->session->userdata('username');
		$data['req'] = $this->mdosen->getReq($id);
		//print_r($data['req']);die;
		$this->template->load('TDosen','dosen/request',$data);
	}



	public function firstLogin() {
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Dosen';
		$this->template->load('TBlank','dosen/password',$data);
	}

	public function changePassword(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Dosen';
		$this->template->load('TDosen','dosen/password',$data);
	}

	public function updatePassword(){
		$newpass = $this->input->post('password');
		$oldpass = '12345678';
		$id = $this->session->userdata('username');
		$data_update = array(
			'password' => md5($newpass),
			'logged'=> 1
 		);

		if ($oldpass !== $newpass){
			$res = $this->mdosen->updateUser($data_update, $id);

			if ($res){
				$this->session->set_flashdata('pesan', 
						'<div class="alert alert-success" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>Password berhasil diubah</strong>
						</div>');
				redirect('dosen');
			} else {
				$this->session->set_flashdata('pesan', 
						'<div class="alert alert-danger" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>Password gagal diubah</strong>
						</div>');
				redirect('dosen/firstlogin');
			}
		} else {
			redirect('dosen/firstlogin');
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}
?>