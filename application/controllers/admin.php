<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		} else if ($this->session->userdata('role') == '3') {
			redirect('dosen');
		} else if ($this->session->userdata('role') == '2') {
			redirect('departemen');
		} else if ($this->session->userdata('role') == '4') {
			redirect('mahasiswa');
		}
		$this->load->helper('text');
		$this->load->model('madmin');
		$this->load->library('email');
	}

	public function index() {
		$data['name'] = $this->session->userdata('role');
		$data['title']='Home Page | Admin';
		//$this->load->view('dashboard1');
		$this->template->load('TAdmin','admin/dash',$data);
	}


	public function addUserDosen(){
		$data['name'] = $this->session->userdata('Role');
		$data['title'] ='User Table | Admin';
		// $data['data'] = $this->madmin->get('user','where grup = 2');
		$this->template->load('TAdmin','admin/add_user',$data);
	}


	public function insert_user(){
		$this->load->helper('string');
		$this->load->library('form_validation');
		$role = $this->input->post('role');
		// var_dump($username);die;
		if ($role=="Dosen") {
			$data = array('success' => false, 'message' => array());
			$this->form_validation->set_rules("NIP", "NIP", "trim|required|integer");
			$this->form_validation->set_rules("nama", "Name", "required");
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if ($this->form_validation->run()) {

				$NIP = $this->input->post('NIP');
				$cek = $this->madmin->getUser($NIP);
				// print_r($NIP);die;
				if (count($cek)==1) {
					$this->session->set_flashdata('pesan',
							'<div class="alert alert-danger" role="alert">
			  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  				<strong>Username sudah terdaftar</strong>
							</div>');
					redirect('admin/addUserDosen');
				} else {
					$dosen = $this->input->post('nama');
					$data_insert_user = array(
						'Username' => $NIP,
						'Password' => $NIP,
						'Role'		 => $role
					);

					$data_insert_dosen = array(
						'NIP'		=>$NIP,
						'dosen'	=>$dosen
					);
					$res = $this->madmin->insert_user($data_insert_user);
					$res2 = $this->madmin->insert_dosen($data_insert_dosen);

					if ($res >=1 || $res2 >=1){

						// $this->sendEmail($username, $pass);
						// $data['success']=true;
						$this->session->set_flashdata('pesan',
							'<div class="alert alert-success" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>User baru berhasil ditambahkan!</strong>
							</div>');
						$this->sendEmail($username);
						redirect('admin/addUserDosen');
					} else {
						echo "<script>alert('User gagal bertambah');</script>";
						$this->session->set_flashdata('pesan',
							'<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>User Gagal bertambah</strong>
							</div>');
						redirect('admin/addUserDosen');
					}
				}
			}else {
				$error="";
				foreach ($_POST as $key => $value) {
					$data['message'][$key] = form_error($key);

				}
				if (isset($data['message']['NIP'])) {
					$error = $data['message']['NIP'];
				}else if (isset($data['message']['Name'])) {
					$error = $data['message']['Name'];
				}
				$this->session->set_flashdata('pesan',
						'<div class="alert alert-danger" role="alert" >
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong style="color:white;">'.$error.'</strong>
						</div>');
				redirect('admin/addUserDosen');
			}
			// echo json_encode($data);

		}
	}

	public function sendEmail($to, $pass){
		$login = base_url('auth');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "staff.ifundip@gmail.com";  // user email address
        $mail->Password   = "staffifundip";            // password in GMail
        $mail->SetFrom('staff.ifundip@gmail.com', 'Admin');  //Who is sending
        $mail->isHTML(true);
        $mail->Subject    = "Your Account is ready!";
        $mail->Body      = "
            <html>
            <head>
                <title>Test</title>
            </head>
            <body>
            <h3>Staff Site Informatics Diponegoro University</h3>
            <br>
            <p>Your account is ready, this is the detail you need for login into our site:</p>
            <p>Your username : $to </p>
            <p>Your pass : $pass </p>
            <p>You can login into our site from this link : $login</p><br>
            <p>With Regards</p>
            </body>
            </html>
        ";
        // Who is addressed the email to
        $mail->AddAddress($to, "Receiver");
        if(!$mail->Send()) {
            return false;
        } else {
            return true;
        }
    }

	public function edit($username){
		$edit = $this->madmin->getUser($username);
		$data['edit'] = array(
			'username' 	=> $edit[0]['username'],	 'name' => $edit[0]['name']
			);
		// $this->template->load('TAdmin');
		$data['name'] = $this->session->userdata('name');
		$data['title']='Home Page | Admin';
		//$this->load->view('dashboard1');
		$this->template->load('TAdmin','admin/edit_view',$data);
	}

	public function edit_user(){
		$name = $this->input->post('name');
		$id = $this->input->post('username');
		$data_update = array(
			'name' => $name
		);

		$res = $this->madmin->updateData($data_update, $id);

		if ($res){
			$this->session->set_flashdata('pesan',
					'<div class="alert alert-success" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Data berhasil diubah</strong>
					</div>');
			redirect('admin/user');
		} else {
			$this->session->set_flashdata('pesan',
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Data berhasil diubah</strong>
					</div>');
			redirect('admin/user');
		}
	}

	public function changePassword(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Admin';
		$this->template->load('TAdmin','admin/password',$data);
	}

	public function updatePassword(){
		$newpass = md5($this->input->post('password'));
		$id = $this->session->userdata('username');
		$data_update = array(
			'password' => $newpass
 		);

		$res = $this->madmin->updateData($data_update, $id);
		if ($res){
			$this->session->set_flashdata('pesan',
					'<div class="alert alert-success" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password berhasil diubah</strong>
					</div>');
			redirect('admin');
		} else {
			$this->session->set_flashdata('pesan',
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password gagal diubah</strong>
					</div>');
			redirect('admin/changePassword');
		}
	}


	public function delete($username){
		$where = array('username' => $username);
		$where2 = array('email1' => $username);
		//hapus semua data yang usernamenya $username
		$del1 = $this->madmin->delete('user', $where);
		$del2 = $this->madmin->delete('pribadi', $where2);
		//hapus hapus hapus



		if ($del1 >= 1){
			$this->session->set_flashdata('pesan',
				'<div class="alert alert-success" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>User berhasil dihapus</strong>
				</div>');
			redirect('admin/user');
		} else {
			$this->session->set_flashdata('pesan',
				'<div class="alert alert-danger" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>User berhasil dihapus</strong>
				</div>');
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}
?>
